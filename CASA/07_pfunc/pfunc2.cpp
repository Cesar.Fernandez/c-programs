#include <stdlib.h>
#include <stdio.h>
#define N 0X100
#define M 5


struct TAlumno {
    char nombre[N];
    char  clase[N];
    int nota_leng;
    int nota_mat;
    int nota_ing;
};

struct TClase {
    struct TAlumno alumno[M];
};

void titulo(){
    system("clear");
    system("toilet -f pagga AULA VIRTUAL");
    printf("\n");
}

void rellenar(struct TClase *p){
    for(int i=0; i<M; i++){
        printf("Como te llamas\n");
        scanf("%s", p->alumno[i].nombre);
        printf("Dime la clase\n");
        scanf("%s", p->alumno[i].clase);
    }
}

void imprimir(struct TClase p){
    for(int i=0; i<M; i++){
        printf("Nombre :%s\n", p.alumno[i].nombre);
        printf("Clase:%s\n", p.alumno[i].clase);
        printf("Nota de Lengua: %i\n", p.alumno[i].nota_leng);
        printf("\n");
    }
}


int main () {
    struct TClase clase;

    clase.alumno[0].nota_leng = 5;
    clase.alumno[1].nota_leng = 8;
    clase.alumno[2].nota_leng = 2;
    clase.alumno[3].nota_leng = 10;
    clase.alumno[4].nota_leng = 3;



    void(*puntero)() = &titulo;

    (*puntero)();

    rellenar(&clase);
    imprimir(clase);







    return EXIT_SUCCESS;
}
