#include <stdlib.h>
#include <stdio.h>
#include <strings.h>
#define N 10

int fibo(int A[]){
    for (int i=0; i<N; i++){
        if(A[0] == 0 || A[1] == 1)
            return 1;
        return fibo(&A[i-1]) + fibo(&A[i-2]);
    }
}
int main () {

    int A[N];

    bzero(A,sizeof(A)/sizeof(int));

    fibo(A);

    for(int i=0; i<N; i++)
        printf("%i ", A[i]);





    return EXIT_SUCCESS;
}
