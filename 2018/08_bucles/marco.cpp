#include <stdio.h>
#include <stdlib.h>

#define N 5

int main () {
    for(int f=0; f<=N; f++){
        for(int c=0; c<=N; c++)
            if(f == 0 || c == 0 || f == N || c == N)
                printf("* ");
            else
                printf("  ");
    
    printf("\n");
}


return EXIT_SUCCESS;

}
