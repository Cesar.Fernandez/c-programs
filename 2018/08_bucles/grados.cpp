#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define N 360

int main () {

    for(int i=0; i<=N; i++)
        printf("%.2lf\n",(double) cos(i) );

	return EXIT_SUCCESS;

}
