#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#define N 10

unsigned dv(unsigned celda[N][N], int f, int c) {
    if (f<0 || c<0 || c>f)
        return (unsigned) 0;
    return celda[f][c];
}

int main () {
    unsigned tri[N][N];
    bzero(tri, sizeof(tri));

    tri[0][0] = 1;
    for(int fila=1; fila<N; fila++)
        for(int colu=1;colu<=fila; colu++)
            tri[fila][colu] = dv(tri, fila-1, colu-1) + dv(tri, fila-1, colu);

    for(int fila=0; fila<N; fila++){
        for(int colu=0; colu<=fila; colu++)
            printf(" %i", tri[fila][colu]);
        printf("\n");
    }
    printf("\n");

	return EXIT_SUCCESS;

}
