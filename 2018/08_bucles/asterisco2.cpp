#include <stdio.h>
#include <stdlib.h>

#define N 5

int main () {
    for(int fila=0; fila<N; fila++){
        for(int colu=0; colu<=fila; colu++)
            printf("*");
        printf("\n");
    }

	return EXIT_SUCCESS;

}
