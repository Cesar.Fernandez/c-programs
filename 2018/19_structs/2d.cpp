#include <stdio.h>
#include <stdlib.h>

#define KLENTO
#define KRAPIDO
#define MAX

struct TVector {
    double x;
    double y;
};

struct TMovil {
    struct TVector pos;
    struct TVector vel;
    void (*mover) (struct TMovil * n);
};

struct TPila {
    struct TMovil *data[MAX];
    int cima;
};

void push ( struct TPila *obj, struct TMovil *n) {
    if (obj->cima >= MAX)
        return;
    obj->data[obj->cima++] = n;
}

void mover_lento (struct Tmovil *n) {
    n->pos.x += n->vel.x * KLENTO;
    n->pos.y += n->vel.y * KLENTO;
};

void mover_rapido (struct TMovil *n) {
    n->pos.x += n->vel.x * KRAPIDO;
    n->pos.y += n->vel.y * KRAPIDO;
};






int main () {

    struct TPila personaje;


    struct TMovil p;

    do {
        struct TMovil nuevo = ( struct TMovil *) malloc (sizeof(struct TMovil));
        iniciar(nuevo);

        push(personaje, nuevo);


    }while(1);


	return EXIT_SUCCESS;

}
