#include <stdio.h>
#include <stdlib.h>

#define M 2
#define N 3
#define K 4

void imprime (double m[][N]){
    for (int i=0; i<M; i++)
        for (int j=0; j<N; j++){
            printf("\t%.2lf" , m[i][j]);
            printf("\n");
        }
}
int main () {

    int matrizB[3][4] = {
        {2,3,4},
        {5,2,6},
        {1,6,3},
        {8,3,2}
    };
    int matrizA[4][2] = {
        {2,5,6,7},
        {5,8,2,1}
    };
    int C[2][3];

    for(int i=0; i<M; i++)
        for(int j=0; j<N; j++){
        C[i][j] = 0;
        for(int k=0;k<K; k++)
        C[i][j] += matrizA[i][k] * matrizB[k][j];
        }


    imprime(C, 2);





    return EXIT_SUCCESS;

}
