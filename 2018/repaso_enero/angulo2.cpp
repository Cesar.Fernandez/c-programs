#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define PI 3.14159265

int main () {

    double vec1Y, vec1X , vec2Y, vec2X;
    double pescalar;
    double cosa , modulo;
    double result;


    printf("Dame la Y del primer vector:");
    scanf(" %lf", &vec1Y);

    printf("Dame la X del primer vector:");
    scanf(" %lf", &vec1X);

    printf("Dame la Y del primer vector:");
    scanf(" %lf", &vec2Y);

    printf("Dame la X del primer vector:");
    scanf(" %lf", &vec2X);


    pescalar = (vec1Y * vec2Y) + (vec1X * vec2X);

    


    modulo = sqrt(pow(vec1Y,2) + pow(vec1X, 2)) * sqrt(pow(vec2Y,2) + pow(vec2X,2));

    cosa = pescalar / modulo;



    result = acos (cosa) * 180.0 / PI;

    printf("El angulo de los dos vectores introducidos anteriormente es %.2lfº \n" , result);
    printf(" Pescalar = %.2lf\n" , pescalar);

    return EXIT_SUCCESS;

}
