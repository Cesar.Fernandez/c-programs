#include <stdio.h>
#include <stdlib.h>

int main () {
    int A, B, C, D, E, F, G, H, I;
    int pri , seg, det;

    printf("Introduzca los tres numeros de la primera fila de la matriz : A B C\n");
    scanf(" %i %i %i", &A, &B, &C);
    printf("Introduca los numeros de la segunda fila : D E F\n");
    scanf(" %i %i %i", &D , &E , &F);
    printf("Introduzca los numeros de la tercera fila : G H I\n");
    scanf(" %i %i %i", &G, &H, &I);

    pri = (A*E*I) + (B*F*G) + (D*H*C);
    seg = (C*E*G) + (B*D*I) + (A*F*H);

    det = pri - seg;

    printf("El determinante es %i\n", det);



	return EXIT_SUCCESS;

}
