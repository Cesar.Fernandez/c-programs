#include <stdio.h>
#include <stdlib.h>

int main () {

    int ax , ay;
    int bx, by;
    int resultado;

    printf("Dame los valores del primer vector : X,Y\n");
    scanf(" %i,%i", &ax , &ay);
    printf("Dame los valores del segundo vector : X,Y\n");
    scanf(" %i,%i", &bx, &by);

    resultado = (ax * by) - (ay * bx);

    printf(" El producto vectorial es %i\n", resultado);

	return EXIT_SUCCESS;

}
