#include <stdio.h>
#include <stdlib.h>

#define N 10
int main () {
    int array[N];

    for(int i=0; i<N; i++)
        array[i] = i+1;

    for(int i=0; i<N; i++)
        printf(" %i",array[i]);
    printf("\n");
    return EXIT_SUCCESS;

}
