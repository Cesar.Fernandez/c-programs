#include <stdio.h>
#include <stdlib.h>

enum Opcion {
  suma,
  resta,
  multiplicacion,
  division,
  OPCIONES
};

const char *texto[] = {
  "suma",
  "resta",
  "multiplicacion",
  "division"
};

double sum (double op1, double op2) { return op1 + op2;}
double res (double op1, double op2) { return op1 - op2;}
double mul (double op1, double op2) { return op1 * op2;}
double div (double op1, double op2) { return op1 / op2;}

int main () {

	return EXIT_SUCCESS;

}
