#include <stdio.h>
#include <stdlib.h>
#define NOMBRE "cancion.txt"
const char * song="\n\
          Don't think sorry is easily said\n\
          Don't try turning tables instead\n\
          You've taken lots of chances before\n\
          But ain't gonna give anymore\n\
\n\
          Don't ask me\n\
          That's how it goes\n\
          'Cause part of me knows what you're thinkin'\n\
\n\
          Don't say words you're gonna regret\n\
          Don't let the fire rush to your head\n\
          I've heard the accusation before\n\
          And I ain't gonna take any more\n\
\n\
          Believe me\n\
          The sun in your eyes\n\
          Made some of the lies worth believing\n\
          \n\
          ";
void print_usage(){
    printf("Esto se usa asi\n");
}

void informo(const char *msg){
    print_usage ();
    fprintf(stderr, "%s\n", msg);
    exit(1);
}
int main () {
    FILE *ficha;

    if ( !(ficha = fopen(NOMBRE, "w")))
        informo("No se ha podido abrir el fichero");

    fprintf(ficha, "%s ", song);

    fclose(ficha);

    return EXIT_SUCCESS;

}
