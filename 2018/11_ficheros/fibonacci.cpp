#include <stdio.h>
#include <stdlib.h>
#define LISTA "fibonacci.dat"


int main () {
    int sucesion[] = {1, 1, 2, 3, 5, 8, 13, 21, 34, 55 };
    FILE *ficha;
    int n = sizeof(sucesion) / sizeof(int);

    if( ! (ficha = fopen(LISTA, "wb"))){
        fprintf(stderr,"No se pudo abrir el fichero");
        return EXIT_FAILURE;
    }


    fwrite(sucesion, sizeof(int), n , ficha);

    fclose(ficha);


	return EXIT_SUCCESS;

}
