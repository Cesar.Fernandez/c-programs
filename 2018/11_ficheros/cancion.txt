
          Don't think sorry is easily said
          Don't try turning tables instead
          You've taken lots of chances before
          But ain't gonna give anymore

          Don't ask me
          That's how it goes
          'Cause part of me knows what you're thinkin'

          Don't say words you're gonna regret
          Don't let the fire rush to your head
          I've heard the accusation before
          And I ain't gonna take any more

          Believe me
          The sun in your eyes
          Made some of the lies worth believing
          
           