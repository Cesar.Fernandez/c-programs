#include <stdio.h>
#include <stdlib.h>

int main () {

    unsigned  int primo[] = {2, 3, 5, 7, 11, 13, 17, 19, 23};
    unsigned elementos = (unsigned)sizeof(primo) / sizeof(int);
    unsigned *peeping = primo;
    char *tom = (char *) primo;
    unsigned **police = &peeping;


    printf( "Localizacion (%p)\n"
            "Elementos: %u [%u .. %u]\n"
            "Tamaño: %lu bytes \n",
            primo,
            elementos,
            primo[0], primo[elementos-1],
            sizeof(primo));

    printf("0: %u\n1: %u\n ", peeping[0], peeping[1]);

    printf("Tamaño: %lu bytes\n", sizeof(peeping));

    for(int i=0; i<sizeof(primo); i++)
        printf("%02X", *(tom + 1));
    printf("\n");

    printf(" Police = %p\n", police);
    printf(" Primo = %p\n", *police);
    printf(" Primo[0] = %u\n", **police);


	return EXIT_SUCCESS;

}
