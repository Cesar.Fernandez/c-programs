#include <stdio.h>
#include <stdlib.h>

#define N 5
int main () {

    int elemento[N];

    elemento[1] = elemento[0] = 1;
    elemento[2] = elemento[1] + elemento[0];
    elemento[3] = elemento[2] + elemento[1];

    return EXIT_SUCCESS;

}
