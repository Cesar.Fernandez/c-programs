#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

using namespace std;

#define NOMBRE "puntuacion.txt"
#define N 0x100


int main () {

    FILE *fichero;
    char cadena[N];
    time_t tiempo = time(NULL);

    fichero = fopen(NOMBRE,"wt");
    printf("Introduce el usuario: \n");
    fgets(cadena, N, stdin);

    fwrite(&cadena,strlen(cadena),1,fichero);
    fprintf(fichero," %s", ctime(&tiempo));



    fclose(fichero);

	return EXIT_SUCCESS;

}
