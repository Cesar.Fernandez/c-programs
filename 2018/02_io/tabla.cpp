#include <stdio.h>
#include <stdlib.h>

#define N 0x100

void pon_titulo(int n){
	char titulo[N];
	sprintf(titulo, "toilet -fpagga --metal Tabla del %i", n);
	system(titulo);
}

int main () {
	int n;
	int producto;

	system("toilet -fpagga --metal Tablas de Multiplicar");


	printf("De que numero quieres la tabla? \n");
	scanf(" %i", &n);
	system("clear");

	pon_titulo(n);

	for( int var = 0; var<11; var++){
		producto = n * var;
		printf("\t %i x %2i = %2i\n" , n , var , producto);
	}


	return EXIT_SUCCESS;

}
