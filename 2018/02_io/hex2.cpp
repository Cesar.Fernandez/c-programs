

#include <stdio.h>
#include <stdlib.h>

int main () {

	int decimal;

	printf("¿Que número quieres pasar a hexadecimal?\n");
	scanf("%i", &decimal);

	printf(" 0x%X\n", decimal);

	return EXIT_SUCCESS;

}
