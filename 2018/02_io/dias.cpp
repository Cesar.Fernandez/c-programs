#include <stdio.h>
#include <stdlib.h>

#define ANYOS 2018
#define MESES 12
#define DIASMES 30

int main () {

	int  anyo , mes , dia;
	int resanyo , resmes , resdia;
	int diastotales;

	printf("Dime tu año de nacimiento.\n");
	printf("Año : ");
	scanf("%i" , &anyo);

	resanyo = ANYOS - anyo;


	printf("Dime tu mes de nacimiento.\n");
	printf("Mes : ");
	scanf("%i" , &mes);

	resmes = MESES - mes;


	printf("Dime tu dia de nacimiento.\n");
	printf("Dia : ");
	scanf("%i" , &dia);

	resdia = DIASMES - dia;



	diastotales = (resanyo * 365) + (resmes * DIASMES) + resdia; 


	printf("Has vivido %i dias\n" , diastotales);



	return EXIT_SUCCESS;

}
